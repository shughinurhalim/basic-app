<?php

namespace app\models;
use yii\base\Model;

class ParticipantForm extends Model
{
	public $name;
	public $email;
	public $valueX;
	public $valueY;
	public $valueZ;
	public $valueW;

	public function rules()
	{
		return [
			[['name', 'email', 'valueX', 'valueY', 'valueZ', 'valueW'], 'required', 'on' => ['insert', 'update']],
			['name', 'string', 'max' => 64, 'on' => ['insert', 'update']],
			['email', 'string', 'max' => 256, 'on' => ['insert', 'update']],
			['email', 'email', 'on' => ['insert', 'update']],
			['valueX', 'integer', 'min' => 1, 'max' => 33, 'on' => ['insert', 'update']],
			['valueY', 'integer', 'min' => 1, 'max' => 23, 'on' => ['insert', 'update']],
			['valueZ', 'integer', 'min' => 1, 'max' => 18, 'on' => ['insert', 'update']],
			['valueW', 'integer', 'min' => 1, 'max' => 13, 'on' => ['insert', 'update']],
		];
	}
}

?>