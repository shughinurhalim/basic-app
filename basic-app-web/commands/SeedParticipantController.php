<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Participant;


 // * @author Qiang Xue <qiang.xue@gmail.com>
 // * @since 2.0
 
class SeedParticipantController extends Controller
{
    public function actionIndex()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++)
        {
            $participant = new Participant();
            $participant->name = $faker->name;
            $participant->email = $faker->email;
            $participant->x = $i + 1;
            $participant->y = $i + 1;
            $participant->z = $i + 1;
            $participant->w = $i + 1;

            $participant->save();
        }

        return;
    }
}
