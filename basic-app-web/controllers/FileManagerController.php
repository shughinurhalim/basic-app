<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;

class FileManagerController extends Controller
{
	public function actionIndex()
	{
		return $this->render('file-manager');
	}

	public function getDirContent($dir, &$result = [])
	{
		$files = scandir($dir);

		foreach ($files as $key => $value) 
		{
			$path = realpath($dir . DIRECTORY_SEPARATOR . $value);

			if ($value == '.gitignore')
			{
				continue;
			}

			if (!is_dir($path)) 
			{
				$result[] = $value;
			} 
			else if ($value != "." && $value != "..") 
			{
				$this->getDirContent($path, $result);
			}
		}

		return $result;
	}

	public function actionSearchFileList()
	{
		$files = $txt = $this->getDirContent(Yii::getAlias('@app') . '/web/tmp/');

		return json_encode($files);
	}

	public function actionDeleteFile()
	{
		$fileName = $this->getParam('fileName');

		if (file_exists(Yii::getAlias('@app') . '/web/tmp/' . $fileName))
		{
			unlink(Yii::getAlias('@app') . '/web/tmp/' . $fileName);

			return json_encode([
				'errNum' => 0,
				'errStr' => 'File deleted successfully'
			]);
		}
		else
		{
			return json_encode([
				'errNum' => 1,
				'errStr' => 'File does not exists'
			]);
		}
	}

	public function checkTags($val)
    {
        $val = strtolower($val);

        if (
            strpos($val, '<script') !== false ||
            strpos($val, '<!--') !== false ||
            strpos($val, '<html') !== false ||
            strpos($val, '<head') !== false ||
            strpos($val, '<body') !== false ||
            strpos($val, '<div') !== false)
        {
            return 1;
        }
    }

    public function checkArrayRecursive($paramValue, $flag)
    {
        if (!is_array($paramValue) && $this->checkTags($paramValue) && $flag == 1)
        {
            die(print('No HTML/Script injection allowed!'));
        }
        else if (is_array($paramValue))
        {
            foreach ($paramValue as $data)
            {
                $this->checkArrayRecursive($data, $flag);
            }
        }

        return;
    }


    public function getParam($paramKey, $flag = 1)
    {
        $paramValue = '';

        if (Yii::$app->request->isGet)
        {
            $paramValue = Yii::$app->request->getQueryParam($paramKey);
        }
        else if (Yii::$app->request->isPost)
        {
            $paramValue = Yii::$app->request->getBodyParam($paramKey);
        }

        $this->checkArrayRecursive($paramValue, $flag);

        return $paramValue;
    }
}