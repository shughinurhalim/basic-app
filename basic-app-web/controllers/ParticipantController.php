<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Participant;
use app\models\ParticipantForm;
use yii\helpers\ArrayHelper;

use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;

class ParticipantController extends Controller
{
    public function actionIndex()
    {
        return $this->render('participant');
    }

    public function actionSearchParticipantList()
    {
        $rows = Participant::find()->orderBy($this->getParam('sort'))->offset($this->getParam('offset'))->limit($this->getParam('limit'))->all();
        $count = Participant::find()->count();

        $data = ArrayHelper::toArray($rows, [
            'app\models\Participant' => [
                'id',
                'name',
                'email',
                'x',
                'y',
                'z',
                'w'
            ],
        ]);

        $result = [
            'rows' => $data,
            'total' => $count
        ];

        return json_encode($result);
    }

    public function actionGetParticipantDetail()
    {
        $rows = Participant::findOne($this->getParam('id'));

        $data = ArrayHelper::toArray($rows, [
            'app\models\Participant' => [
                'id',
                'name',
                'email',
                'x',
                'y',
                'z',
                'w'
            ],
        ]);

        return json_encode($data);
    }

    public function actionInsertParticipant()
    {
        $participantForm = new ParticipantForm();

        $participantForm->name = $this->getParam('name');
        $participantForm->email = $this->getParam('email');
        $participantForm->valueX = $this->getParam('valueX');
        $participantForm->valueY = $this->getParam('valueY');
        $participantForm->valueZ = $this->getParam('valueZ');
        $participantForm->valueW = $this->getParam('valueW');
        $participantForm->scenario = 'insert';

        if ($participantForm->validate())
        {
            $participant = new Participant();

            $participant->name = $this->getParam('name');
            $participant->email = $this->getParam('email');
            $participant->x = $this->getParam('valueX');
            $participant->y = $this->getParam('valueY');
            $participant->z = $this->getParam('valueZ');
            $participant->w = $this->getParam('valueW');

            $participant->save();

            $result = [
                'errNum' => 0,
                'errStr' => 'Participant inserted successfully'
            ];

            return json_encode($result);
        }
        else
        {
            $result = [
                'errNum' => 1,
                'errStr' => $participantForm->errors
            ];

            return json_encode($result);
        }
    }

    public function actionUpdateParticipant()
    {
        $participantForm = new ParticipantForm();

        $participantForm->name = $this->getParam('name');
        $participantForm->email = $this->getParam('email');
        $participantForm->valueX = $this->getParam('valueX');
        $participantForm->valueY = $this->getParam('valueY');
        $participantForm->valueZ = $this->getParam('valueZ');
        $participantForm->valueW = $this->getParam('valueW');
        $participantForm->scenario = 'update';

        if ($participantForm->validate())
        {
            $participant = Participant::findOne($this->getParam('id'));

            $participant->name = $this->getParam('name');
            $participant->email = $this->getParam('email');
            $participant->x = $this->getParam('valueX');
            $participant->y = $this->getParam('valueY');
            $participant->z = $this->getParam('valueZ');
            $participant->w = $this->getParam('valueW');

            $participant->save();

            $result = [
                'errNum' => 0,
                'errStr' => 'Participant updated successfully'
            ];

            return json_encode($result);
        }
        else
        {
            $result = [
                'errNum' => 1,
                'errStr' => $participantForm->errors
            ];

            return json_encode($result);
        }
    }

    public function actionDeleteParticipant()
    {
        $participant = Participant::findOne($this->getParam('id'));
        $participant->delete();

        $result = [
            'errNum' => 0,
            'errStr' => 'Participant deleted successfully'
        ];

        return json_encode($result);
    }

    public function actionGenerateReport()
    {
        $participant = Participant::findOne($this->getParam('id'));
        $valInt = (int) (0.4 * $participant->x) + (0.6 * $participant->y / 2);
        $valNum = (int) (0.3 * $participant->z) + (0.7 * $participant->w / 2);

        if ($valInt > 5)
        {
            $valInt = 5;
        }
        else if ($valInt < 1)
        {
            $valInt = 1;
        }

        if ($valNum > 5)
        {
            $valNum = 5;
        }
        else if ($valNum < 1)
        {
            $valNum = 1;
        }

        $data = [];
        $dataInt = ['Aspek Intelegensi'];
        $dataNum = ['Aspek Numerical Ability'];

        for ($i = 0; $i < 5; $i++)
        {
            if ($valInt == $i + 1)
            {
                array_push($dataInt, 'v');
            }
            else
            {
                array_push($dataInt, '');
            }
        }

        for ($i = 0; $i < 5; $i++)
        {
            if ($valNum == $i + 1)
            {
                array_push($dataNum, 'v');
            }
            else
            {
                array_push($dataNum, '');
            }
        }

        array_push($data, $dataInt);
        array_push($data, $dataNum);

        $fileName = 'Report_' . $participant->name . '.xlsx';
        $dir = \Yii::getAlias('@app') . '/web/tmp/';

        if (!is_dir($dir))
        {
            mkdir($dir, 0775, true);
        }

        $dataTitle = [
            ['Nama', $participant->name],
            ['Email', $participant->email]
        ];
        
        $dataHeader = ['Aspek', '1', '2', '3', '4', '5'];

        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToFile($dir . '/' . $fileName);
        
        $titleStyle = (new StyleBuilder())
            ->setFontBold()
            ->setFontSize(15)
            ->build();
            
        $paramStyle = (new StyleBuilder())
            ->setFontSize(11)
            ->setFontColor(Color::BLUE)
            ->build();
            
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->setFontSize(11)
            ->build();

        $sheet = $writer->getCurrentSheet();
        $writer->addRowsWithStyle($dataTitle, $titleStyle);
        $writer->addRow(['']);
        $writer->addRowWithStyle($dataHeader, $headerStyle);
        $writer->addRows($data);

        $writer->close();

        return Yii::$app->response->sendFile(Yii::getAlias('@app') . '/web/tmp/' . $fileName);
    }

    public function checkTags($val)
    {
        $val = strtolower($val);

        if (
            strpos($val, '<script') !== false ||
            strpos($val, '<!--') !== false ||
            strpos($val, '<html') !== false ||
            strpos($val, '<head') !== false ||
            strpos($val, '<body') !== false ||
            strpos($val, '<div') !== false)
        {
            return 1;
        }
    }

    public function checkArrayRecursive($paramValue, $flag)
    {
        if (!is_array($paramValue) && $this->checkTags($paramValue) && $flag == 1)
        {
            die(print('No HTML/Script injection allowed!'));
        }
        else if (is_array($paramValue))
        {
            foreach ($paramValue as $data)
            {
                $this->checkArrayRecursive($data, $flag);
            }
        }

        return;
    }


    public function getParam($paramKey, $flag = 1)
    {
        $paramValue = '';

        if (Yii::$app->request->isGet)
        {
            $paramValue = Yii::$app->request->getQueryParam($paramKey);
        }
        else if (Yii::$app->request->isPost)
        {
            $paramValue = Yii::$app->request->getBodyParam($paramKey);
        }

        $this->checkArrayRecursive($paramValue, $flag);

        return $paramValue;
    }
}
