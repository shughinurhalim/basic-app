<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

$this->title = 'File Manager';
$this->params['breadcrumbs'][] = $this->title;
AppAsset::register($this);
?>

<div class="row">
	<div class="col-sm-3">
		<div class="float-left">
			<h1><?= Html::encode($this->title) ?></h1>
		</div>
	</div>
</div>
<br>
<div class="table-responsive">
	<table class="table" id="tblFileManager" width="100%">
		<thead>
			<th data-field="name" data-sortable="false" data-align="center"><?= Yii::t('app', 'File Name') ?></th>
			<th data-formatter="actionFormatter"><?= Yii::t('app', 'Action') ?></th>
		</thead>
		<tbody id="tBodyFile">

		</tbody>
	</table>
</div>

<script type="text/javascript">
	$(document).ready(function()
	{
		searchFile();
	})

	function searchFile()
	{
		$.ajax({
			type 		: 'POST',
			url			: '<?= Url::to(['file-manager/search-file-list']) ?>',
			data		: [],
			dataType	: 'JSON',
			success		: function (response)
			{
				if (response.length != 0)
				{
					var element = '';

					for (var i = 0; i < response.length; i++)
					{
						element += '\
							<tr>\
								<td>' + response[i] + '</td>\
								<td><button class="btn btn-sm btn-danger" onclick="deleteFile(\'' + response[i] + '\')">Delete</button></td>\
							</tr>\
						';
					}

					$('#tBodyFile').html(element);
				}
				else
				{
					$('#tBodyFile').html('<tr><th>No Data Found</th></tr>');
				}
			}
		})
	}

	function deleteFile(fileName)
	{
		var data = {
			'fileName' : fileName
		}

		BootstrapModalWrapperFactory.confirm
		({
			title          : '<?= Yii::t('app', 'Confirm') ?>',
			message        : '<?= Yii::t('app', 'Are you sure you want to delete this file?') ?>' ,
			onConfirmAccept: function()
			{
				$.ajax({
					type 		: 'POST',
					url			: '<?= Url::to(['file-manager/delete-file']) ?>',
					data		: data,
					dataType	: 'JSON',
					success		: function (response)
					{
						BootstrapModalWrapperFactory.alert({
							title: '<?= Yii::t('app', 'Message') ?>',
							message: nl2br(msgConverter(response)),
							buttons: [
								{
									label: 'OK',
									cssClass: 'btn btn-primary',
									action: function (modalWrapper, button, buttonData, originalEvent)
									{
										modalWrapper.hide();
										searchFile();
									}
								},
							]
						});
					}
				})
			}
		});
	}

	function msgConverter(response)
	{
	    var output = '';

	    if (typeof(response.errStr) == 'object')
	    {
	        var property;

	        for (property in response.errStr)
	        {
	            output += response.errStr[property] + '<br>';
	        }
	    }
	    else
	    {
	        output += response.errStr;
	    }

	    if (response.errNum != 0)
	    {
	        output = '<p class="text-danger">' + output + '</p>';
	    }

	    return output;
	}

	function nl2br(str, is_xhtml) 
	{
	    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
	    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
	}

	function actionFormatter(value, row, index)
	{
		element = '\
			<button class="btn btn-sm btn-primary" onclick="generateReport(' + row.id + ')">Report</button>\
			<button class="btn btn-sm btn-secondary" onclick="getParticipantDetail(' + row.id + ')">Update</button>\
			<button class="btn btn-sm btn-danger" onclick="deleteParticipant(' + row.id + ')">Delete</button>\
		';

		return element;
	}
</script>