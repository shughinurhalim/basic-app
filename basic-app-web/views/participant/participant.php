<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

$this->title = 'Participant';
$this->params['breadcrumbs'][] = $this->title;
AppAsset::register($this);
?>



<div class="row">
	<div class="col-sm-3">
		<div class="float-left">
			<h1><?= Html::encode($this->title) ?></h1>
		</div>
	</div>
	<div class="col-sm-6"></div>
	<div class="col-sm-3" style="justify-content: right; align-items: center; display: flex">
		<div class="float-right">
			<button id="btnAdd" class="btn btn-sm btn-primary">Add Participant</button>
			<button id="btnFileManager" class="btn btn-sm btn-secondary">File Manager</button>
		</div>
	</div>
</div>
<br>
<div class="table-responsive">
	<table class="table" id="tblParticipantList" width="100%">
		<thead>
			<th data-field="name" data-sortable="false" data-align="center"><?= Yii::t('app', 'Name') ?></th>
			<th data-field="email" data-sortable="false" data-align="center"><?= Yii::t('app', 'Email') ?></th>
			<th data-field="x" data-sortable="false" data-align="center"><?= Yii::t('app', 'Value X') ?></th>
			<th data-field="y" data-sortable="false" data-align="center"><?= Yii::t('app', 'Value Y') ?></th>
			<th data-field="z" data-sortable="false" data-align="center"><?= Yii::t('app', 'Value Z') ?></th>
			<th data-field="w" data-sortable="false" data-align="center"><?= Yii::t('app', 'Value W') ?></th>
			<th data-formatter="actionFormatter"><?= Yii::t('app', 'Action') ?></th>
		</thead>
	</table>
</div>
<div class="modal fade" id="modalForm" aria-hidden="true" aria-labelledby="formLabel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="formLabel"></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="formData" class="form" autocomplete="off">
					<div class="form-group row required">
						<label class="col-sm-4 col-form-label"><?= Yii::t('app', 'Name') ?></label>
						<div class="col-sm-8">
							<input id="inputName" name="name" type="text" class="form-control form-control-sm" placeholder="<?= Yii::t('app', 'Name') ?>">
						</div>
					</div>

					<div class="form-group row required">
						<label class="col-sm-4 col-form-label"><?= Yii::t('app', 'Email') ?></label>
						<div class="col-sm-8">
							<input id="inputEmail" name="email" type="text" class="form-control form-control-sm" placeholder="<?= Yii::t('app', 'Email') ?>">
						</div>
					</div>

					<div class="form-group row required">
						<label class="col-sm-4 col-form-label"><?= Yii::t('app', 'Value X') ?></label>
						<div class="col-sm-8">
							<input id="inputX" name="x" type="text" class="form-control form-control-sm" placeholder="<?= Yii::t('app', 'Value X') ?>">
						</div>
					</div>

					<div class="form-group row required">
						<label class="col-sm-4 col-form-label"><?= Yii::t('app', 'Value Y') ?></label>
						<div class="col-sm-8">
							<input id="inputY" name="y" type="text" class="form-control form-control-sm" placeholder="<?= Yii::t('app', 'Value Y') ?>">
						</div>
					</div>

					<div class="form-group row required">
						<label class="col-sm-4 col-form-label"><?= Yii::t('app', 'Value Z') ?></label>
						<div class="col-sm-8">
							<input id="inputZ" name="z" type="text" class="form-control form-control-sm" placeholder="<?= Yii::t('app', 'Value Z') ?>">
						</div>
					</div>

					<div class="form-group row required">
						<label class="col-sm-4 col-form-label"><?= Yii::t('app', 'Value W') ?></label>
						<div class="col-sm-8">
							<input id="inputW" name="w" type="text" class="form-control form-control-sm" placeholder="<?= Yii::t('app', 'Value W') ?>">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-secondary" onclick="resetForm();"><?= Yii::t('app', 'Reset') ?></button>
				<button class="btn btn-sm btn-primary" onclick="submitForm()" type="submit"><?= Yii::t('app', 'Submit') ?></button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	self._oldData;
	self._action;

	$(document).ready(function()
	{
		searchParticipant();

		$('#btnAdd').on('click', function()
		{
			$('#formLabel').html('<?= Yii::t('app', 'Add Participant') ?>');
			self._action = 'add';
			resetForm();
			$('#modalForm').modal('show');
		});

		$('#btnFileManager').on('click', function()
		{
			window.location.href = '<?= Yii::$app->getUrlManager()->createUrl('file-manager') ?>';
		})
	});

	function submitForm()
	{
		if (self._action == 'add')
		{
			insertParticipant();
		}
		else if (self._action == 'edit')
		{
			updateParticipant();
		}
	}

	function resetForm()
	{
		$('#formData').trigger('reset');

		if (self._action == 'edit')
		{
			$('#inputName').val(self._oldData.name);
			$('#inputEmail').val(self._oldData.email);
			$('#inputX').val(self._oldData.x);
			$('#inputY').val(self._oldData.y);
			$('#inputZ').val(self._oldData.z);
			$('#inputW').val(self._oldData.w);
		}
	}

	function insertParticipant()
	{
		BootstrapModalWrapperFactory.confirm
		({
			title          : '<?= Yii::t('app', 'Confirm') ?>',
			message        : '<?= Yii::t('app', 'Are you sure you want to add this participant?') ?>' ,
			onConfirmAccept: function()
			{
				data = {
					name : $('#inputName').val(),
					email : $('#inputEmail').val(),
					valueX : $('#inputX').val(),
					valueY : $('#inputY').val(),
					valueZ : $('#inputZ').val(),
					valueW : $('#inputW').val()
				};

				$.ajax({
					type 		: 'POST',
					url			: '<?= Url::to(['participant/insert-participant']) ?>',
					data		: data,
					dataType	: 'JSON',
					success		: function (response)
					{
						BootstrapModalWrapperFactory.alert({
							title: '<?= Yii::t('app', 'Message') ?>',
							message: nl2br(msgConverter(response)),
							buttons: [
								{
									label: 'OK',
									cssClass: 'btn btn-primary',
									action: function (modalWrapper, button, buttonData, originalEvent)
									{
										modalWrapper.hide();
										if (response.errNum == 0)
										{
											resetForm();
											$('#modalForm').modal('hide');
											searchParticipant();
										}
									}
								},
							]
						});
					}
				})
			}
		});
	}

	function updateParticipant()
	{
		BootstrapModalWrapperFactory.confirm
		({
			title          : '<?= Yii::t('app', 'Confirm') ?>',
			message        : '<?= Yii::t('app', 'Are you sure you want to update this participant?') ?>' ,
			onConfirmAccept: function()
			{
				data = {
					id : self._oldData.id,
					name : $('#inputName').val(),
					email : $('#inputEmail').val(),
					valueX : $('#inputX').val(),
					valueY : $('#inputY').val(),
					valueZ : $('#inputZ').val(),
					valueW : $('#inputW').val()
				};

				$.ajax({
					type 		: 'POST',
					url			: '<?= Url::to(['participant/update-participant']) ?>',
					data		: data,
					dataType	: 'JSON',
					success		: function (response)
					{
						BootstrapModalWrapperFactory.alert({
							title: '<?= Yii::t('app', 'Message') ?>',
							message: nl2br(msgConverter(response)),
							buttons: [
								{
									label: 'OK',
									cssClass: 'btn btn-primary',
									action: function (modalWrapper, button, buttonData, originalEvent)
									{
										modalWrapper.hide();
										if (response.errNum == 0)
										{
											resetForm();
											$('#modalForm').modal('hide');
											searchParticipant();
										}
									}
								},
							]
						});
					}
				})
			}
		});
	}

	function deleteParticipant(id)
	{
		BootstrapModalWrapperFactory.confirm
		({
			title          : '<?= Yii::t('app', 'Confirm') ?>',
			message        : '<?= Yii::t('app', 'Are you sure you want to delete this participant?') ?>' ,
			onConfirmAccept: function()
			{
				data = {
					id : id
				};

				$.ajax({
					type 		: 'POST',
					url			: '<?= Url::to(['participant/delete-participant']) ?>',
					data		: data,
					dataType	: 'JSON',
					success		: function (response)
					{
						BootstrapModalWrapperFactory.alert({
							title: '<?= Yii::t('app', 'Message') ?>',
							message: nl2br(msgConverter(response)),
							buttons: [
								{
									label: 'OK',
									cssClass: 'btn btn-primary',
									action: function (modalWrapper, button, buttonData, originalEvent)
									{
										modalWrapper.hide();
										if (response.errNum == 0)
										{
											searchParticipant();
										}
									}
								},
							]
						});
					}
				})
			}
		});
	}

	function searchParticipant()
	{
		$('#tblParticipantList').bootstrapTable('destroy');
		$('#tblParticipantList').bootstrapTable
		({
			method          : 'POST',
			contentType     : 'application/x-www-form-urlencoded',
			sortName        : 'name',
			sortOrder       : 'asc',
			pagination      : true,
			sidePagination  : 'server',
			showRefresh     : false,
			singleSelect    : true,
			clickToSelect   : true,
			url             : '<?= Yii::$app->getUrlManager()->createUrl('participant/search-participant-list') ?>',
			responseHandler : function(res)
			{
				return res;
			},
			onLoadSuccess   		: function()
			{
				
			},
			onCheck         		: function()
			{
				
			},
			onUncheck       		: function()
			{
				
			},
			queryParams     		: function(p)
			{
				return p;
			},
			formatNoMatches : function()
			{
				return '<?= Yii::t('app', 'Data Not Found') ?>';
			}
		});
	}

	function getParticipantDetail(id)
	{
		data = {
			id : id
		}

		$.ajax({
			type 		: 'POST',
			url			: '<?= Url::to(['participant/get-participant-detail']) ?>',
			data		: data,
			dataType	: 'JSON',
			success		: function (response)
			{
				self._oldData = response;
				self._action = 'edit';
				resetForm();
				$('#formLabel').html('<?= Yii::t('app', 'Update Participant') ?>');
				$('#modalForm').modal('show');
			}
		})
	}

	function generateReport(id)
	{
		window.open('<?= Url::to(['participant/generate-report']) ?>' + '?id=' + id);

		// data = {
		// 	id : id
		// }

		// $.ajax({
		// 	type 		: 'POST',
		// 	url			: '<?= Url::to(['participant/generate-report']) ?>',
		// 	data		: data,
		// 	dataType	: 'JSON',
		// 	success		: function (response)
		// 	{
		// 		console.log(response);

		// 		element = '<a href="' + response.filePath + '">link</a>';
		// 		$('#generateLink').html(element);
		// 	}
		// })
	}

	function msgConverter(response)
	{
	    var output = '';

	    if (typeof(response.errStr) == 'object')
	    {
	        var property;

	        for (property in response.errStr)
	        {
	            output += response.errStr[property] + '<br>';
	        }
	    }
	    else
	    {
	        output += response.errStr;
	    }

	    if (response.errNum != 0)
	    {
	        output = '<p class="text-danger">' + output + '</p>';
	    }

	    return output;
	}

	function nl2br(str, is_xhtml) 
	{
	    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
	    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
	}

	function actionFormatter(value, row, index)
	{
		element = '\
			<button class="btn btn-sm btn-primary" onclick="generateReport(' + row.id + ')">Report</button>\
			<button class="btn btn-sm btn-secondary" onclick="getParticipantDetail(' + row.id + ')">Update</button>\
			<button class="btn btn-sm btn-danger" onclick="deleteParticipant(' + row.id + ')">Delete</button>\
		';

		return element;
	}
</script>