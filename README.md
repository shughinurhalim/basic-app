# basic-app

Basic app to add, update, delete, and view participant report

Used environment:
 - Xampp7
 - PHP 7.0.27
 - MySql

**Note**
To run the web application just type `localhost/basic-app/basic-app-web/web` after turning on the xampp7 apache server.<br />
To run data migration just type `yii migrate` to create table `participant`. <br />
To run data seed just type `php yii seed-participant` to populate participant data. <br />
You can edit db configuration in file `config/db` (default config in file db is schema yii2basic with username root and password 000000)




